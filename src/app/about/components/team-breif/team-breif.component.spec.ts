import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamBreifComponent } from './team-breif.component';

describe('TeamBreifComponent', () => {
  let component: TeamBreifComponent;
  let fixture: ComponentFixture<TeamBreifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamBreifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamBreifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
